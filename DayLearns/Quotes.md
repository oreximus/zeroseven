### **QUOTES COLLECTION**

- A problem is an emotional state. Without emotions, it is just a situation.

- Be So Good that, They can't Ignore You!

- Progress is noticeable when the question that tortured us has lost its meaning.

- Focusing is the purposeful and deliberate alignment to a specific goal.

- Never say 'No', never say 'I cannot', for you are infinite. **All the power is within you. You can do anything.** -By: Swami Vivekanand

- If you aim at nothing, you hit nothing! - from Shang Chi Movie
