# 4 Points

## [Youtube Source](https://www.youtube.com/watch?v=fFmi34XO0Cc)

## 1. Understanding the Pattern of the Field

- Whatever you are into related to your career specifically, just try to understand the very fundamentals of it, and analyze it for your own sake.

- Evaluate various factors of it, like the required time investment, topics of interests and incentives or motives to do it! Besides this there are many more things that you can address, but the important thing is that you are aware with the facts related to your desired field of interest.

## 2. Interospection

- Understaning the field is not going to help you, you also need to understand yourself, You need to **Introspect Yourself**!

- It'll help you to aware with your current conditions and make you more focused about your actions.

- Improvements and further enhacement can be done effectively once we know enough about overselves.

- Removal of distraction and avoiding unnecessary actions can actually help through this.

## 3. Dealing with Situation

- Everyone has different circumstances, a different situation to deal with and we just have to find the most appropriate way to do it.

- Situations have root causes and the solution to it is most likely to appear from those causes.

- Learn to be more patient and observable when it comes to deal with the situations.

- Always chose the most wise and calm way to resolve the issues around you.

### 4. Personality

- It's very important for us to make a good balance with our life and health.

- From Mental as well as Physical point of view, we should stay fit and be able to work with our maximum potential.

- Personality not only help us to stay confident in most places but also help to work in a very precise manner.


