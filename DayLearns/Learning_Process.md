# Learning Process; Source: HTB ACADEMY 

## Way of thinking 

Learn how to **find**, **choose** and **adapt** the information we need.  

## Think Outside the Box 

There is no limitations for thinking.

## Way of Learning

 - Thinking outside the box means seeing things outside of the limitations placed on us. This means we have to be able to "pivot."

 - **A problem is an emotional state. Without emotions, it is just a situation.**

 - In other words, frustration and confusion come with the point of view we are looking at. The learning process is not just a theoretical and practical part. It is also our learning process and progress that largely depends on our emotional state. If we feel good and we know we will reach our goal, we will be successful.

 - Another essential part that makes you successful is that you **know your goal**.

## Learning Efficiency

 - The primary and most difficult objective we must overcome is the **combination** of our knowledge, adaptation, and new information.

 - It often is not easy to find the information we need. First, we have to find out what kind of information we need.
 
 	- What do we already know?
  	- What do we not know yet?


- However, first of all, we have to fail. It is an **unavoidable** and **essential** part of **learning**. This is one of the parts of the learning process which make us successful. Experience is built on failures. It explains that we know how to handle differently. Sometimes adverse, situations where something does not work as expected.

 - To be good at something means we know what we are doing. If we know **what we are doing**, that means that we are **experienced** with this topic.

 - Experience means we have a vast **repertoire** in this field. Repertoire comes from **associations** and **practical experience**. When we say practical experience, we want to know how much we have to practice to become competent at a specific task.

[IT IS POSSIBLE TO LEARN SOMETHING NEW IN 20 HOURS]: https://ideas.ted.com/dont-have-10000-hours-to-learn-something-new-thats-fine-all-you-need-is-20-hours/

 - It's similar to **Pareto Principle**, or the **80/20 rule**

 - As Josh Kaufman explained, we can become excellent pretty fast. This is the so-called **learning curve**, including active and passive learning. These active and passive learning types can be found in the: 

[Learning Pyramid]: https://en.wikipedia.org/wiki/Learning_pyramid

![Edgar Dale's cone of learning](images/Edgar_Dale's_cone_of_learning.png)

## Learning Types

 - The Learning Pyramid can be represented in many different ways. It describes the learning efficiency of different types of practice.

### Passive learning

 - If we follow the Learning Pyramid while going through the modules just by reading, we will learn only about **10%** of the whole penetration testing experience. By watching some demonstrations, we will not learn more than **30%.**

### Active Learning

 - By using this type of active learning, we collect up to **50%** experience. Before we can discuss our results with others, we should practice on our own. So while we practice, our learning experience grows to **75%**.

 - To learn to discern such information, we need a **repertoire** which we collect by practicing.

 - it is essential to understand the context of the topic we are researching.

 - Efficiency depends not only on the quality of information we find but on the usage of that information.

 - Moreover, it depends on our **motivation**, **focus**, and **our goal**.

 - **Progress is noticeable when the question that tortured us has lost its meaning.**

## Documentation

 - The purpose of documentation is to present the information we have obtained in a comprehensible and easy way to reproduce a specific activity.
 - Therefore the essential characteristics of documentation are:
 	
	1. Overview
	2. Structure
	3. Clarity
 - No matter whom the documentation is intended for, here are some guidelines we can follow:

	1. It is beneficial to put ourselves in the position of our readers. This will make it much easier for us to design the documentation.
	2. Avoid repetition and ambiguity.
	3. Make documentation as easy to read as possible. No one wants to read the documentation that is difficult to understand or follow.

## Oreganization

 - There are many different management techniques and methods that we can use. These include:
 	
	- Scrum
	- Agile
	- ToDo-Lists
	- Bullet Journal and more.

## Focus

 - **Attention** refers to the momentum, as it is happening right now, and you are reading this text. However, the **focus** is on the topic you are dealing with at the moment.

 - **Focusing is the purposeful and deliberate alignment to a specific goal.**

## Attention

 - **Attention is influenced by your interests, needs, personal attitudes, beliefs, orientations, goals, and experiences.**

 - Attention is an independent **mental process** that takes place subconsciously.

 -  We will not be able to absorb all the information at once. We will often come back to topics and repeat what we are missing. This is a normal process. We must understand how to divide our attention.

 - We can document it, and after one week, we will be able to see an interesting pattern. If we want to approach this on a more scientific level, we can add the following points to our documentation to get a better insight into it:

	- current emotional state (calm, nervous, worried, happy, depressed, relaxed, etc.)
	- the previous flow of the day so far (also with one word)
	- place of work
	- working hours
	- duration
	- sleep
	- inserted breaks
	- duration of the breaks
	- and anything else we can think of.

## Comfort

 - There is so-called **Yerkes-Dodson** law, which describes the cognitive performance as a function of the level of stress/nervousness. The performance curve for this is also very individual, as it depends strongly on emotional and motivational factors and is divided into four sections.

 - The most used presentation of this law and the performance process is the Hebbian version.

![Yerkes-Dodson Law](images/NEW_yerkes-dodson-law.png)

 - **Mistakes are an essential part of the learning process**

![The Comfort Zone](images/NEW_The-Comfort-Zone-diagram.png)

## Handling Frustration

 - Frustration is an emotional reaction to an event, situation, or condition that occurs in the form of disappointment or powerlessness. Most often, such a feeling occurs in varying intensity, depending on expectations or desires.

 - There are two different types of frustration:
	- One is caused by **external influences** such as negative opinions of superiors, 
	- and the other is caused by **inner frustration**, caused by conscious or somewhat subconscious thought processes.

 - Most people are not aware that feelings **reflect subconscious thoughts** and thought processes. That is why we can understand quite well how we think from our feelings. Often it helps to listen to our thoughts from a 3rd-person perspective or imagine that our best friend expresses these thoughts. With that, we gain some distance from the feeling of being affected by it, which makes it easier to construct an objective opinion and judgment about it.

 - In order to express frustration tolerance in this way, it is crucial to know where it comes from. Let us take a look at the following diagram:

![New Vision](images/NEW_Vision.png)

 - Do not forget that this feeling of frustration is **temporary**. Instead, over time, we will become calmer in reacting and dealing with such stressful situations, which in turn will strengthen our self-confidence. We can control our inner frustration. The frustration of the external factors, however, can hardly be controlled.


## Learning Progress

 - An important aspect to be considered in the learning process is **progress**.

 - In order to see our progress, two specific states are compared, including a specific time window between the learning process. In other words, we compare our knowledge from the past with the present and try to keep track of the progress to give ourselves the confirmation that we have achieved something new.
