# Itellectual Properties

Intellectual property (IP) refers to creations of the mind, such as inventions; literary and artistics works, designs; and symbols, names and images used in commerce.

|   | **Utility patent** | **Design patent** | **Plant patent** | **Copyright** | **Trade secret** | **Trademark** |
|---|--------------------|-------------------|------------------|---------------|------------------|---------------|
| **What Protected** | Inventions | Ornamental (non functional) Designs | Newly invented plants | Books, photos, music, fine art, graphic images, videos, films, architecture, computer programs | Formulas, methods, devices, or compilations of information which is confidential and gives a business an advantage | Words, symbols, logos, designs, or slogans that identify and distinguish products or services.|
| **Examples** | iPod, chemical fertilizer, process of manipulating genetic traits in mice | Unique shape of electric guitar, design for a lamp | Flowering plants, fruits trees, hybrid plants | Michael Jackson's Thriller (music, artwork and video), Windows operating system | Coca-Cola formula, survey methods used by a polister, new invention for which patent application has not been filed | Coca-Cola name and distinctive logo, Pilsbury doughboy character |
| **Duration of Protection** | 20 years from the date of filing regular patent application | 15 years | 20 year from filing date | The life of the author plus 70 year (or some works, 95 years from pub., and others 120 years from creation) | As long as informations remains confidential and functions as a trade secret | As long as mark is in continuos use in connection with goods or services - renew by year 10, then every 10 years. |

## Trademarks

- Allow consumers to identify the source or producer of different products and services - helps their buying decisions

- Encourage trademark owners to provide goods and services of consistent quality and to build goodwill in the trademark

**Federally registered trademarks:**

- Right to enforce nationally and bring legal action in federal courts

- Use of federal trademark registration symbol ®

- Right to record mark with customs

- Serve as basis for foreign filing

- Publication in U.S. trademark database


