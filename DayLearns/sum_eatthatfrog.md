## 'EAT THAT FROG' BY: Brian Tracy

- **The first rule of frog eating is this: If you have to eat two frogs, eat the ugliest one first.
**(This is another way of saying that if you have two important tasks before you, start with the biggest, hardest, and most important task first. Discipline yourself to begin immediately and then to persist until the task is complete before you go on to something else.)

- Think of this as a test. Treat it like a personal challenge. Resist the temptation to start with the easier task. Continually remind yourself that one of the most important decisions you make each day is what you will do immediately and what you will do later, if you do it at all.

- **The second rule of frog eating is this: If you have to eat a live frog at all, it doesn't pay to sit and look at 
