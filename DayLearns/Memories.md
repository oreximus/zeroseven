# 7 Secrets to Memorise Things Quicker than others | How to Memorize better?

## BY: AMAN DHATTARWAL
## [YT](https://youtu.be/dRVvlwPpYLQ)

**STRONG FOCUS**
- Recalling
- Rest
- Exercise

1. REPETITION
2. NOTES MAKING
    - Diagrams and Flowcharts
3. EMOTIONS IN STORY
4. GOOD SLEEP
5. LEARN AS A TEACHER
6. RECALLING

![img01](images/img01.png)
