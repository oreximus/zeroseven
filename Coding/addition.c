#include <stdio.h>

int main(){

  float a, b, c;

  printf("Enter the first Number to Add:\t\n");
  scanf("%f", &a);

  printf("Enter the Second Number to Add:\t\n");
  scanf("%f", &b);

  c = a + b;

  printf("The sum of %f and %f is: %.2f\n", a,b,c);

  return 0;

}
