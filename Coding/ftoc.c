#include <stdio.h>

int main() {
  
  float far, cel;

  printf("Enter the farenheit value:\t\n");
  scanf("%f", &far);

  cel = 5 * (far - 32) / 9;

  printf("Celcius value of Far is: %.2f\n", cel);

  return 0;
}
