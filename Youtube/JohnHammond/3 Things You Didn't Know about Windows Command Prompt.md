
## COMSPEC Variable and What you can do with it!

## 👉[Resource](https://www.youtube.com/watch?v=p97cmJgChK4)👈

## Environment Variable: COMPSEC

- On **CMD**:

```
echo %COMPSEC%
```

- On **Windows Terminal**, opened with **Powershell**:

```
echo $env:COMPSEC
```

- Drive Directories

```
dir env:\
```

### Using COMSPEC to run Arbitrary Commands:

- Setting different Value of COMSPEC:

```
set COMSPEC=calc.exe
```

- then when echoing that Variable:

```
echo %COMSPEC%
```

**output:**

![img01](imgs/img01.png)

- And echoing some random text along with piping with the other command:

```
echo "some random text"|clip
```

**output:**

![img02](imgs/img02.png)

- It invokes calc.exe, that we've setup.

### What extra you could do with this?

- When trying all this on CMD it shows this on the process monitor:

![img03](imgs/img03.png)

- Which showing that some of the flags **/S /D /c** are being used here and Notepage is executed with **"notepad"**, inside double quotes!

- So, when trying it with some extra parameters, it generates this error!

- command on cmd:
```
echo 'a'|whoami
```

- by monitoring on process monitor:
![img04](imgs/img04.png)

let's try controlling the arguments:

### Controlling the Arguments:

- Trying all this on powershell, using within Windows Terminal:

**setting COMSPEC Variable value:**

```
$env:COMSPEC="notepad.exe"
```

**Executing it:**

```
cmd /c "break|rem"
```

![img05](imgs/img05.png)

- Now this time it didn't shows the **notepad** into **double quotes**

### Facts leading to manipulate the Arguments:

### FACT #1: Command Prompt has the maximum string length that we can on it is 8191 characters.

### FACT #2: Even though the Win32 limitation for environment variables is 32767 characters, Command Prompt ignores any environment variables that are inherited from the parent process and are longer than its own limitations of 8191 characters ( as appropriate to the operating system). 

👉[**reference**](https://learn.microsoft.com/en-us/troubleshoot/windows-client/shell-experience/command-line-string-limitation)👈

- Creating powershell script to manipulate the arguments:

```
$env:COMPSEC="notepad.exe" + " "*(8191-11)
cmd.exe /c "break|rem"
```

- Three things:
	- About this COMSPEC Variable
	- Two other are the Facts about CMD.